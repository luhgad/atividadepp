/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.hibernate.spatial.tutorials.testejsf;

import java.io.Serializable;
import java.util.Locale;
import javax.faces.context.FacesContext;

/**
 *
 * @author Luciana
 */
public class Internacionalizacao implements Serializable{

    private String pais = "";
    private String linguagem = "";

    public Internacionalizacao() {
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public String getLinguagem() {
        return linguagem;
    }

    public void setLinguagem(String linguagem) {
        this.linguagem = linguagem;
    }

    private void mudarLocalidade(Locale locale) {
        FacesContext.getCurrentInstance().getViewRoot().setLocale(locale);
    }

    public String mudarIdioma() {
        if (!"".equals(pais)) {
            this.mudarLocalidade(new Locale(linguagem, pais));
        } else {
            this.mudarLocalidade(new Locale(linguagem));
        }

        return null;
    }

}
