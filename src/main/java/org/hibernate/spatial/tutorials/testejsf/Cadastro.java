package org.hibernate.spatial.tutorials.testejsf;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import org.hibernate.spatial.tutorials.testejsf.Pessoa;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Luciana
 */
@Stateless
public class Cadastro {

    @PersistenceContext(unitName = "persistencejsf")
    private EntityManager manager;

    public Cadastro() {
    }

    public void salvar(Pessoa pessoa) {
        manager.persist(pessoa);

    }

    public void atualizar(Pessoa pessoa) {
        manager.merge(pessoa);
    }

    public List<Pessoa> listar() {
        return manager.createQuery("Select p from Pessoa", Pessoa.class).getResultList();

    }

    public void remover(Pessoa pessoa) {
        manager.remove(manager.merge(pessoa));
    }

}
