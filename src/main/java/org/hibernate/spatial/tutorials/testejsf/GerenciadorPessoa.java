/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.hibernate.spatial.tutorials.testejsf;

import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

/**
 *
 * @author Luciana
 */
@Named(value = "gerenciadorPessoa")
@SessionScoped
public class GerenciadorPessoa implements Serializable {

    private Pessoa pessoa;
    @EJB
    private Cadastro cadastro;

    public GerenciadorPessoa() {
        pessoa = new Pessoa();

    }

    public Pessoa getPessoa() {
        return pessoa;
    }

    public String salvar() {
        cadastro.salvar(pessoa);
        Pessoa p = new Pessoa();
        return null;
    }

    public String atualizar(Pessoa pessoa) {
        cadastro.atualizar(pessoa);
        Pessoa p = new Pessoa();
        return null;
    }

    public List<Pessoa> listar() {
        List<Pessoa> pessoas = cadastro.listar();
        return pessoas;
    }
    
    public String revomer() {
        cadastro.remover(pessoa);
        Pessoa p = new Pessoa();
        return null;
    }


}
